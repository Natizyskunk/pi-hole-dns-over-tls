# pi-hole-dns-over-tls
Pi-hole as All-Around DNS Solution: DNS Over TLS, Simple ENCRYPTED recursive caching DNS, TCP port 853

## Setting up Pi-hole as a recursive DNS server solution
We will use unbound, a secure open source recursive DNS server primarily developed by NLnet Labs, VeriSign Inc., Nominet, and Kirei. The first thing you need to do is to install the recursive DNS resolver:
```bash
sudo apt install -y unbound dnsutils
```
**Important:** Download the current root hints file (the list of primary root servers which are serving the domain "." - the root domain). Update it roughly every six months. Note that this file changes infrequently.**
```bash
wget -O /var/lib/unbound/root.hints https://www.internic.net/domain/named.root
```


## Configure unbound
Get ca-root-nss.crt certificate and unbound config file.
```bash
wget -O /usr/local/share/ca-certificates/ca-root-nss.crt https://gitlab.com/Natizyskunk/pi-hole-dns-over-tls/raw/master/certificates/ca-root-nss.crt
wget -O /etc/unbound/unbound.conf.d/pi-hole.conf https://gitlab.com/Natizyskunk/pi-hole-dns-over-tls/raw/master/unbound.conf.d/pi-hole.conf
```

Start your local recursive server and test that it's operational:
```bash
sudo systemctl restart pihole-FTL
sudo systemctl start unbound
sudo systemctl enable unbound
dig pi-hole.net @127.0.0.1 -p 5353
```
The first query may be quite slow, but subsequent queries, also to other domains under the same TLD, should be fairly quick.

## Test validation
You can test DNSSEC validation using
```bash
dig sigfail.verteiltesysteme.net @127.0.0.1 -p 5353 #should give a status report of SERVFAIL and no IP address.
dig sigok.verteiltesysteme.net @127.0.0.1 -p 5353 #should give a status report of NOERROR plus an IP address.
```
The first command should give a status report of SERVFAIL and no IP address. The second should give NOERROR plus an IP address.

## Configure Pi-hole
Finally, configure Pi-hole to use your recursive DNS server: <br>
![RecursiveResolver.png](https://docs.pi-hole.net/images/RecursiveResolver.png "Logo RecursiveResolver")

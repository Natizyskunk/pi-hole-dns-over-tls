server:
    unwanted-reply-threshold: 10000000
    # If no logfile is specified, syslog is used
    # logfile: "/var/log/unbound/unbound.log"
    val-log-level: 2
    verbosity: 1
    use-syslog: yes
    log-queries: yes
    log-servfail: yes

    statistics-interval: 0
    extended-statistics: yes
    # statistics-cumulative: no

    interface: 0.0.0.0
    interface: ::0
    # interface: 127.0.0.1
    # interface: ::1
    # interface-automatic: no

    access-control: 0.0.0.0/0 refuse
    access-control: 10.0.0.0/8 allow
    access-control: 127.0.0.0/8 allow
    access-control: 192.168.0.0/16 allow
    access-control: ::0/0 refuse
    access-control: ::1 allow
    access-control: fd00::/8 allow
    access-control: fe80::/10 allow
    # access-control: 0.0.0.0/0 allow
    # access-control: ::0/0 allow

    port: 5353
    do-ip4: yes
    do-udp: yes
    do-tcp: yes

    # May be set to yes if you have IPv6 connectivity
    do-ip6: no
    # prefer-ip6: no
    tls-upstream: yes
    # ssl-upstream: yes

    aggressive-nsec: yes
    cache-min-ttl: 1800
    cache-max-ttl: 86400
    rrset-roundrobin: yes

    # tls-service-key: "/etc/letsencrypt/live/votre_domaine/privkey.pem"
    # tls-service-pem: "/etc/letsencrypt/live/votre_domaine/fullchain.pem"
    tls-cert-bundle: "/usr/local/share/ca-certificates/ca-root-nss.crt"
    # tls-cert-bundle: "/etc/ssl/certs/ca-certificates.crt"
    tls-port: 853

    username: unbound

    # chroot: /var/lib/unbound
    # directory: /var/lib/unbound
    # pidfile: /var/run/unbound.pid
    hide-identity: yes
    hide-version: yes

    # Use this only when you downloaded the list of primary root servers!
    root-hints: "/var/lib/unbound/root.hints"

    harden-algo-downgrade: no

    # Trust glue only if it is within the servers authority
    harden-glue: yes

    harden-below-nxdomain: yes

    # Require DNSSEC data for trust-anchored zones, if such data is absent, the zone becomes BOGUS
    harden-dnssec-stripped: yes

    # add validation
    module-config: "validator iterator"

    # Don't use Capitalization randomization as it known to cause DNSSEC issues sometimes
    # see https://discourse.pi-hole.net/t/unbound-stubby-or-dnscrypt-proxy/9378 for further details
    use-caps-for-id: no

    val-clean-additional: yes

    # Reduce EDNS reassembly buffer size.
    # Suggested by the unbound man page to reduce fragmentation reassembly problems
    edns-buffer-size: 1472

    minimal-responses: yes

    # Perform prefetching of close to expired message cache entries
    # This only applies to domains that have been frequently queried
    prefetch: yes

    # One thread should be sufficient, can be increased on beefy machines. In reality for most users running on small networks or on a single machine it should be unnecessary to seek performance enhancement by increasing num-threads above 1.
    num-threads: 1

    # Increase cache size to store more records and allow each thread to
    # serve an increased number of concurrent client requests.
    msg-cache-slabs: 2
    rrset-cache-slabs: 2
    infra-cache-slabs: 2
    key-cache-slabs: 2
    msg-cache-size: 128M
    rrset-cache-size: 256M
    key-cache-size: 512M
    outgoing-range: 974
    # num-queries-per-thread: 4096

    # Faster UDP with multithreading (only on Linux).
    so-reuseport: yes

    # Ensure kernel buffer is large enough to not lose messages in traffic spikes
    so-rcvbuf: 1m

    # Ensure privacy of local IP ranges
    private-address: 10.0.0.0/8
    private-address: 127.0.0.0/8
    private-address: 192.168.0.0/16
    private-address: ::1
    private-address: fd00::/8
    private-address: fe80::/10

    # forward-addr format must be ip "@" port number "#" followed by the valid public hostname
    # in order for unbound to use the tls-cert-bundle to validate the dns server certificate.
    forward-zone:
        name: "."
        forward-tls-upstream: yes
        forward-addr: 1.1.1.1@853
        forward-addr: 1.0.0.1@853
        forward-addr: 2606:4700:4700::1111@853
        forward-addr: 2606:4700:4700::1001@853

remote-control:
    control-enable: no
